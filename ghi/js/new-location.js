window.addEventListener("DOMContentLoaded", async () => {
  const url = "http://localhost:8000/api/states/";
  const response = await fetch(url);

  if (response.ok) {
    // Figure out what to do when the response is bad
    const data = await response.json();
    // Get the select tag element by its id 'state'
    const selectTag = document.getElementById("state");
    // For each state in the states property of the data
    for (let state of data.states) {
      // create option using DOM
      const newOption = new Option();
      newOption.value = state.abbreviation;
      newOption.innerHTML = state.name;
      selectTag.add(newOption, undefined);
      // Get the select tag element by its id 'state'
      // For each state in the states property of the data
      // Create an 'option' element
      // Set the '.value' property of the option element to the
      // state's abbreviation
      // Set the '.innerHTML' property of the option element to
      // the state's name
      // Append the option element as a child of the select tag
    }
    const formTag = document.getElementById("create-location-form");
    formTag.addEventListener("submit", async (event) => {
      event.preventDefault();
      const formData = new FormData(formTag);
      const json = JSON.stringify(Object.fromEntries(formData));

      const locationURL = "http://localhost:8000/api/locations/";
      const fetchConfig = {
        method: "post",
        body: json,
        headers: {
          "Content-Type": "application/json",
        },
      };
      const response = await fetch(locationURL, fetchConfig);
      if (response.ok) {
        formTag.reset();
        const newLocation = await response.json();
        console.log(newLocation);
      }
    });
  }
});
