window.addEventListener("DOMContentLoaded", async () => {
  const url = "http://localhost:8000/api/locations/";
  try {
    const response = await fetch(url);

    if (response.ok) {
      // Figure out what to do when the response is bad
      const data = await response.json();
      // Get the select tag element by its id 'state'
      const selectTag = document.getElementById("location");
      // For each state in the states property of the data
      for (let location of data.locations) {
        // create option using DOM
        let option = document.createElement("option");
        option.value = location.id;
        option.innerHTML = location.name;
        selectTag.appendChild(option);
        // Get the select tag element by its id 'state'
        // For each state in the states property of the data
        // Create an 'option' element
        // Set the '.value' property of the option element to the
        // state's abbreviation
        // Set the '.innerHTML' property of the option element to
        // the state's name
        // Append the option element as a child of the select tag
      }
    }
  } catch (e) {
    console.error("Couldn't load locations");
  }
  const formTag = document.getElementById("create-conference-form");
  formTag.addEventListener("submit", async (event) => {
    event.preventDefault();
    const formData = new FormData(formTag);
    const json = JSON.stringify(Object.fromEntries(formData));

    const conferenceURL = "http://localhost:8000/api/conferences/";
    const fetchConfig = {
      method: "post",
      body: json,
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(conferenceURL, fetchConfig);
    if (response.ok) {
      formTag.reset();
      const newConference = await response.json();
      console.log(newConference);
    }
  });
});
