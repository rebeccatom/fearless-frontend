function createCard(name, description, pictureUrl, startDate, endDate) {
  return `
      <div class="col-4">
      <div class="card shadow mb-3">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <p class="card-text">${description}</p>
          <div class="card-footer">
          <p class="card-text"><small class="text-muted">${startDate} - ${endDate}</small></p>
          </div>
        </div>
      </div>
      </div>
    `;
}

window.addEventListener("DOMContentLoaded", async () => {
  const url = "http://localhost:8000/api/conferences/";

  try {
    const response = await fetch(url);

    if (!response.ok) {
      // Figure out what to do when the response is bad
    } else {
      const data = await response.json();

      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const name = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const startDate = details.conference.starts;
          const endDate = details.conference.ends;
          const html = createCard(
            name,
            description,
            pictureUrl,
            startDate,
            endDate
          );
          const column = document.querySelector(".row");
          column.innerHTML += html;
        }
      }
    }
  } catch (e) {
    // Figure out what to do if an error is raised
  }
});
